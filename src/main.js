import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'
import router from './router'
// import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'
import './assets/less/index.less'
import './assets/font/ALfont/iconfont'
// axios.defaults.baseURL = '/api' 
// axios.defaults.baseURL = '/sever' 
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import axios from 'axios'
//qs
import qs from 'qs'

// import 'element-plus/lib/theme-chalk/index.css'
// import 'emoji-mart/css/emoji-mart.css'
// import { Picker } from 'vue-emoji-mart'


// 初始化项目
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
// axios.defaults.baseURL = 'http://localhost:8081/' // 设置请求的默认地址
// app.component('emoji-picker', Picker)
app.config.globalProperties.$axios=axios
app.config.globalProperties.$qs=qs

app.use(router)
app.mount('#app')
// 完整引入，将所有组件引入(我的电脑该方法会出bug)
// 可在组建任何位置使用
// app.use(ElementPlus);
